import java.util.Objects

object LLVM {
  sealed trait Name {
    def name: String
    override def toString: String = name
  }
  case object NoName extends Name {
    override def name: String = ""
  }
  case class Local(name: String) extends Name {
    if(!name.startsWith("%")) throw new IllegalArgumentException(s"Local name must start with '%'")
  }
  case class Global(name: String) extends Name {
    if(!name.startsWith("@")) throw new IllegalArgumentException(s"Global name must start with '@'")
  }

  sealed trait Type
  case class TInt(bits: Int) extends Type {
    override def toString: String = s"i$bits"
  }
  val i1: TInt = TInt(1)
  val i8: TInt = TInt(8)
  val i32: TInt = TInt(32)

  // boolean represented as i32 with 0=false, all else true
  val boolean: TInt = TInt(32)

  trait FloatType extends Type
  case object THalf extends FloatType {
    override def toString: String = "half"
  }
  case object TFloat extends FloatType {
    override def toString: String = "float"
  }
  case object TDouble extends FloatType {
    override def toString: String = "double"
  }
  case object TFp128 extends FloatType {
    override def toString: String = "fp128"
  }
  case object TX86_fp80 extends FloatType {
    override def toString: String = "x86_fp80"
  }
  case object TPpc_fp128 extends FloatType {
    override def toString: String = "ppc_fp128"
  }

  case class TArray(ty: Type, numElements: Int) extends Type {
    override def toString: String = s"[$numElements x $ty]"
  }
  case class TStruct() extends Type {
    override def toString: String = "void"
  }
  case class TString(nc: Int) extends Type {
    override def toString: String = s"[$nc x i8]"
  }

  case object TNull extends Type {
    override def toString: String = ""
  }
  case object TVoid extends Type {
    override def toString: String = "void"
  }
  case class TRef(ref: Name) extends Type {
    override def toString: String = ref.toString
  }

  case class FunctionDef(rt: Type, name: Global, params: Seq[Param], body: Seq[BasicBlock])  {
    override def toString: String =
      s"""define $rt $name(${params.mkString(", ")}) {
         |${body.mkString("\n\n")}
         |}
         |""".stripMargin
  }

  case class Param(t: Type, name: Local) {
    override def toString: String = s"$t $name"
  }

  case class BasicBlock(label: Option[String], insts: Seq[Instruction], term: Terminator, noIndent: Boolean = false) {
    override def toString: String = {
      val labS = if (label.isEmpty) "" else s"${label.get}:\n"
      labS + insts.map(i => (if(noIndent) "" else "  ") + s"$i\n").mkString + "  " + term
    }

    def first: Instruction =
      if (insts.nonEmpty) insts.head else term

    def map(f: Instruction => Instruction): BasicBlock =
      BasicBlock(label, insts.map(f), f(term).asInstanceOf[Terminator])
  }


  trait Value {
    def ty: Type
  }
  case class Ref(name: Name, ty: Type) extends Value {
    Objects.requireNonNull(name)
    override def toString: String = name.toString
  }
  case object Void extends Value {
    override def toString: String = ""
    override def ty: Type = TVoid
  }
  case object Null extends Value {
    override def toString: String = ""
    override def ty: Type = TNull
  }

  trait Constant extends Value
  case class ConstantInt(i: Int, ty: Type) extends Constant {
    override def toString: String = i.toString
  }
  case class ConstantFP(d: Double, ty: Type = TDouble) extends Constant {
    override def toString: String = d.toString
  }
  case class ConstantString(s: String, ty: Type) extends Constant {
    override def toString: String = s
  }

  case class FunRef(ty: Type, name: Global) extends Value {
    override def toString: String = name.toString
  }

  /**
   * https://llvm.org/doxygen/classllvm_1_1Instruction.html
   */

  trait Instruction {
    var targetRegister: Name = NoName
    def named(name: Name): this.type = {
      this.targetRegister = name
      this
    }
    def prefixed(s: Any): String = targetRegister match {
      case NoName => s.toString
      case _ => s"${targetRegister} = $s"
    }
    def yieldsVoid: Boolean = false
  }

  trait VoidInstruction extends Instruction {
    override def yieldsVoid: Boolean = true
  }

  case class Alloca(ty: Type, numElements: Int = 1) extends Instruction {
    //override def toString: String = prefixed(s"alloca $ty, $ty $numElements")
    override def toString: String = prefixed(s"alloca $ty")
  }
  case class AllocaByRef(ref: Name, numElements: Int = 1) extends Instruction {
    override def toString: String = prefixed(s"alloca $ref")
  }
  case class Store(ty: Type, v: Value, name: Name) extends VoidInstruction {
    override def toString: String = prefixed(s"store $ty $v, $ty* $name")
  }
  case class Load(ty:Type, name: Name) extends Instruction {
    override def toString: String = prefixed(s"load $ty, $ty* $name")
  }

  case class Call(fun: FunRef, args: Seq[Value]) extends Instruction {
    override def toString: String = prefixed(s"call ${fun.ty} $fun(${args.map(arg => s"${arg.ty} $arg").mkString(", ")})")
  }

  case class CastToFp(v: Value) extends Instruction {
    override def toString: String = prefixed(s"sitofp ${v.ty} $v to double")
  }

  /**
   * https://llvm.org/doxygen/classllvm_1_1BinaryOperator.html
   */
  case class BinaryOperator(op: BinOp, ty: Type, v1: Value, v2: Value) extends Instruction {
    override def toString: String = prefixed(s"$op $ty $v1, $v2")
  }

  sealed trait BinOp
  case object Add extends BinOp {
    override def toString: String = "add"
  }
  case object Sub extends BinOp {
    override def toString: String = "sub"
  }
  case object Mul extends BinOp {
    override def toString: String = "mul"
  }
  case object Div extends BinOp {
    override def toString: String = "sdiv"
  }
  case object FAdd extends BinOp {
    override def toString: String = "fadd"
  }
  case object FSub extends BinOp {
    override def toString: String = "fsub"
  }
  case object FMul extends BinOp {
    override def toString: String = "fmul"
  }
  case object FDiv extends BinOp {
    override def toString: String = "fdiv"
  }
  case object And extends BinOp {
    override def toString: String = "and"
  }
  case object Or extends BinOp {
    override def toString: String = "or"
  }
  case object Xor extends BinOp {
    override def toString: String = "xor"
  }

  case class CmpInst(isFp: Boolean, pred: Predicate, ty: Type, v1: Value, v2: Value) extends Instruction {
    override def toString: String = prefixed(s"${if(isFp) "fcmp" else "icmp"} $pred $ty $v1, $v2")
  }
  case class FCmpInst(pred: Predicate, ty: Type, v1: Value, v2: Value) extends Instruction {
    override def toString: String = prefixed(s"fcmp $pred $ty $v1, $v2")
  }

  sealed trait Predicate
  case object ICMP_EQ extends Predicate {
    override def toString: String = "eq"
  }
  case object ICMP_NE extends Predicate {
    override def toString: String = "ne"
  }
  case object ICMP_SLT extends Predicate {
    override def toString: String = "slt"
  }
  case object ICMP_SLE extends Predicate {
    override def toString: String = "sle"
  }
  case object FCMP_OEQ extends Predicate {
    override def toString: String = "oeq"
  }
  case object FCMP_ONE extends Predicate {
    override def toString: String = "one"
  }
  case object FCMP_OLT extends Predicate {
    override def toString: String = "olt"
  }
  case object FCMP_OLE extends Predicate {
    override def toString: String = "ole"
  }

  // Choose v if predecessor basic block has given label
  case class Alt(v: Value, label: String) {
    override def toString: String = s"[ $v , %$label ] "
  }
  // Represents a phi-function. Chooses the value based on the executed predecessor basic block
  case class Phi(ty: Type, alts: Seq[Alt]) extends Instruction {
    override def toString: String = prefixed(s"phi $ty " + alts.mkString(", "))
  }

  // Collection instructions
  case class Struct(tys: Type*) extends Instruction {
    override def toString: String = prefixed(s"type { ${tys.mkString(", ")} }")
  }
  case class GEP(ty: Type, name: Name, ind: Value, pInd: Int = 0) extends Instruction {
    override def toString: String = prefixed(s"getelementptr $ty, $ty* $name, i32 $pInd, i32 $ind")
  }
  case class ExtractValue(ty: Type, v: Value, ind: ConstantInt) extends Instruction {
    override def toString: String = prefixed(s"extractvalue $ty $v, $ind")
  }
  case class CreateString(ty: TString, s: String) extends Instruction {
    override def toString: String = prefixed(s"private constant $ty " + "c\"" + s"$s" + "\\0A\\00\"")
  }

  /**
   * Instructions that terminate a basic block
   */
  trait Terminator extends VoidInstruction

  case class Br(label: String) extends Terminator {
    override def toString: String = prefixed(s"br label %$label")
  }
  case class BrIf(v: Value, thnLabel: String, elsLabel: String) extends Terminator {
    override def toString: String = prefixed(s"br i1 $v, label %$thnLabel, label %$elsLabel")
  }
  case class Return(ty: Type, v: Value) extends Terminator {
    override def toString: String = prefixed(s"ret $ty $v")
  }
  case class ReturnVoid() extends Terminator {
    override def toString: String = prefixed("ret void")
  }
  case class EmptyTerm() extends Terminator {
    override def toString: String = ""
  }

}
