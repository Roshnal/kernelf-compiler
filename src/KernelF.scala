import LLVM.Type

object KernelF {
  sealed trait Instr {
    def freevars: Set [String]
  }

  sealed trait Function extends Instr
  case class Fun(name: String, params: scala.collection.immutable.Seq[(String, Type)], body: Exp) extends Function {
    override def freevars: Set[String] = body.freevars
  }

  sealed trait Exp extends Instr
  case class NumberLit(n: Any) extends Exp {
    override def freevars: Set[String] = Set()
  }
  case class IntegerLit(i: Int) extends Exp {
    override def freevars: Set[String] = Set()
  }
  case class RealLit(d: Double) extends Exp {
    override def freevars: Set[String] = Set()
  }
  case class StringLit(s: String) extends Exp {
    override def freevars: Set[String] = Set()
  }
  case class BooleanLit(b: Boolean) extends Exp {
    override def freevars: Set[String] = Set()
  }
  trait Collection extends Exp
  case class Tuple[T <: Exp](exps: T*) extends Collection {
    override def freevars: Set[String] = exps.map(e => e.freevars).reduce(_++_)
  }
  case class List[T <: Exp](exps: T*) extends Collection {
    override def freevars: Set[String] = exps.map(e => e.freevars).reduce(_++_)
  }
  case object Null extends Exp {
    override def freevars: Set[String] = Set()
  }

  case class Closure(params: scala.collection.immutable.Seq[(String, Type)], body: Exp) extends Exp {
    override def freevars: Set[String] = body.freevars
  }

  case class Var(x: String) extends Exp {
    override def freevars: Set[String] = Set(x)
  }

  case class Add(e1: Exp, e2: Exp) extends Exp {
    override def freevars: Set[String] = e1.freevars ++ e2.freevars
  }
  case class Sub(e1: Exp, e2: Exp) extends Exp {
    override def freevars: Set[String] = e1.freevars ++ e2.freevars
  }
  case class Mul(e1: Exp, e2: Exp) extends Exp {
    override def freevars: Set[String] = e1.freevars ++ e2.freevars
  }
  case class Div(e1: Exp, e2: Exp) extends Exp {
    override def freevars: Set[String] = e1.freevars ++ e2.freevars
  }

  case class And(e1: Exp, e2: Exp) extends Exp {
    override def freevars: Set[String] = e1.freevars ++ e2.freevars
  }
  case class Or(e1: Exp, e2: Exp) extends Exp {
    override def freevars: Set[String] = e1.freevars ++ e2.freevars
  }
  case class Xor(e1: Exp, e2: Exp) extends Exp {
    override def freevars: Set[String] = e1.freevars ++ e2.freevars
  }

  case class Eq(e1: Exp, e2: Exp) extends Exp {
    override def freevars: Set[String] = e1.freevars ++ e2.freevars
  }
  case class NotEq(e1: Exp, e2: Exp) extends Exp {
    override def freevars: Set[String] = e1.freevars ++ e2.freevars
  }
  case class LT(e1: Exp, e2: Exp) extends Exp {
    override def freevars: Set[String] = e1.freevars ++ e2.freevars
  }
  case class LEq(e1: Exp, e2: Exp) extends Exp {
    override def freevars: Set[String] = e1.freevars ++ e2.freevars
  }
  case class GT(e1: Exp, e2: Exp) extends Exp {
    override def freevars: Set[String] = e1.freevars ++ e2.freevars
  }
  case class GEq(e1: Exp, e2: Exp) extends Exp {
    override def freevars: Set[String] = e1.freevars ++ e2.freevars
  }

  case class If(cond: Exp, thn: Exp, els: Exp) extends Exp {
    override def freevars: Set[String] = cond.freevars ++ thn.freevars ++ els.freevars
  }

  case class Call(name: String, args: Exp*) extends Exp {
    override def freevars: Set[String] = args.map(e => e.freevars).reduce(_++_)
  }

  case class At(col: Exp, ind: IntegerLit) extends Exp {
    override def freevars: Set[String] = ind.freevars ++ col.freevars
  }
  case class IsEmpty(col: Collection) extends Exp {
    override def freevars: Set[String] = col.freevars
  }
  case class Size(col: Collection) extends Exp {
    override def freevars: Set[String] = col.freevars
  }


  sealed trait Stm extends Instr {
    def boundvars: Set[String]
  }
  case class Run(e: Exp) extends Stm {
    override def freevars: Set[String] = e.freevars
    override def boundvars: Set[String] = Set()
  }
  case class Assign(x: String, e: Exp) extends Stm {
    override def freevars: Set[String] = e.freevars
    override def boundvars: Set[String] = Set(x)
  }
  case class Seq(s1: Stm, s2: Stm) extends Stm {
    override def freevars: Set[String] = s1.freevars ++ (s2.freevars diff s1.boundvars)
    override def boundvars: Set[String] = s1.boundvars ++ s2.boundvars
  }
}
