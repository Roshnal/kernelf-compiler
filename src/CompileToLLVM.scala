import LLVM._

import scala.collection.mutable

trait CompileToLLVM {
  private val blocks: mutable.Buffer[BasicBlock] = mutable.Buffer()
  private var currentLabel: Option[String] = None
  private var currentCode: mutable.Buffer[Instruction] = mutable.Buffer()
  private var codeExt: mutable.Buffer[Instruction] = mutable.Buffer()
  private var unnamedTemporariesCount = 0

  def getBlocks: Seq[BasicBlock] = blocks.toSeq

  def getExtBlock: BasicBlock = BasicBlock(None, codeExt.toSeq, EmptyTerm(), noIndent = true)

  def clear(): Unit = {
    blocks.clear()
    currentCode.clear()
    codeExt.clear()
    currentLabel = None
    unnamedTemporariesCount = 0
  }

  def startBlock(label: String): Unit = {
    if(currentLabel.nonEmpty || currentCode.nonEmpty) throw new IllegalStateException()
    currentLabel = Some(label)
  }

  def endBlock(term: Terminator): Unit = {
    blocks += BasicBlock(currentLabel, currentCode.toSeq, term)
    currentLabel = None
    currentCode.clear()
  }

  def emitInstruction(inst: Instruction, x: Local = null, ext: Boolean = false): Local =
    if(inst.yieldsVoid) {
      currentCode += inst
      null
    } else {
      val xvar = if(x != null) x else {
        unnamedTemporariesCount += 1
        Local("%" + unnamedTemporariesCount)
      }
      inst.targetRegister = xvar
      if(ext) codeExt += inst else currentCode += inst
      xvar
    }

  def emitInstruction(inst: Instruction, x: Global): Global =
    if(inst.yieldsVoid) {
      codeExt += inst
      null
    } else {
      val xvar = if(x != null) x else {
        unnamedTemporariesCount += 1
        Global("@" + unnamedTemporariesCount)
      }
      inst.targetRegister = xvar
      codeExt += inst
      xvar
    }
}
