import LLVM._

import scala.collection.mutable.ListBuffer

class CompileKernelF extends CompileToLLVM {
  val gensym: Gensym = new Gensym(Set())
  var varMem: Map[String, Local] = Map()
  var refMem: Map[Local, Local] = Map()
  var tyMem: Map[String, Type] = Map()
  var strTyMem: Map[Local, Seq[Type]] = Map()

  def compileProg(p: Seq[_]): String = {
    val msBuf: ListBuffer[KernelF.Stm] = ListBuffer.empty
    p.foreach {
      case e: KernelF.Exp => msBuf += KernelF.Run(e)
      case s: KernelF.Stm => msBuf += s
      case _ =>
    }
    val mainStm = msBuf.reduceRight((a, b) => KernelF.Seq(a, b))
    val funs = p.filter(ins => ins.isInstanceOf[KernelF.Fun]).asInstanceOf[Seq[KernelF.Fun]]

    Seq(funs.map(compileProg).mkString(""), compileProg(mainStm)).mkString("")
  }

  def compileProg(p: KernelF.Instr): String = {
    gensym.clear()
    clear()

    val vars = p match {
      case fun: KernelF.Fun =>
        fun.params.foreach(p => tyMem += p._1 -> p._2)
        val fparams = fun.params.map(p => p._1).toSet[String]
        (fparams ++ (p.freevars diff fparams)).toSeq
      case _ => p.freevars.toSeq
    }
    val params = vars.map(x => Param(tyMem.get(x) match {
      case Some(ty) => ty
      case None =>
        tyMem += x -> i32 // arbitrary default type. Should use proper inference based on usage
        i32
    }, Local("%" + x)))

    varMem = vars.map { x =>
      val mem = emitInstruction(Alloca(tyMem(x)))
      emitInstruction(Store(tyMem(x), Ref(Local("%" + x), tyMem(x)), mem))
      x -> mem
    }.toMap

    val res = p match {
      case p: KernelF.Fun => compile(p.asInstanceOf[KernelF.Fun].body)
      case p: KernelF.Exp => compile(p.asInstanceOf[KernelF.Exp])
      case p: KernelF.Stm => compile(p.asInstanceOf[KernelF.Stm])
    }
    val retTy = res.ty match {
      case TStruct()|TString(_) =>
        endBlock(ReturnVoid())
        TVoid
      case _ =>
        endBlock(Return(res.ty, res))
        res.ty
    }

    Seq(getExtBlock, FunctionDef(retTy, Global("@" + (p match {
      case p: KernelF.Fun =>
        tyMem += Global("@" + p.name).toString -> retTy
        p.name
      case _ => "main"
    })), params, getBlocks)).mkString("\n")
  }

  def compile(s: KernelF.Stm): Value = s match {
    case KernelF.Run(e) =>
      compile(e)
    case KernelF.Assign(x, e) =>
      val v = compile(e)
      v.ty match {
        case TRef(_) =>
          if(!varMem.contains(x)) varMem += x -> Local(v.toString)
          Void
        case _ =>
          val mem = varMem.get(x) match {
            case Some(mem) => mem
            case None =>
              val mem = emitInstruction(Alloca(v.ty))
              varMem += x -> mem
              tyMem += x -> v.ty
              mem
          }
          emitInstruction(Store(v.ty, v, mem))
          Void
      }

    case KernelF.Seq(s1, s2) =>
      compile(s1)
      compile(s2)
  }

  def compile(e: KernelF.Exp): Value = e match {
    /**  Literals & Collections  */
    case KernelF.NumberLit(n) =>
      n match {
        case i: Int => compile(KernelF.IntegerLit(i))
        case d: Double => compile(KernelF.RealLit(d))
        case _ => throw new IllegalArgumentException("NumberLit must represent a numeric value")
      }
    case KernelF.IntegerLit(i) =>
      ConstantInt(i, i32)
    case KernelF.RealLit(d) =>
      ConstantFP(d, TDouble)
    case KernelF.StringLit(s) =>
      val ty = TString(s.length + 2)
      val str = emitInstruction(CreateString(ty, s), Global(gensym.fresh("@.str.")))
      tyMem += str.toString -> ty
      Ref(str, ty)
    case KernelF.BooleanLit(b) =>
      ConstantInt(if(b) 1 else 0, i32)
    case KernelF.Null =>
      Void

    case KernelF.Tuple(exps @_*) =>
      val vexps = exps.map(compile)
      val str = emitInstruction(Struct(vexps.map(e => e.ty):_*), Local(gensym.fresh("%Struct.")), ext=true)
      val alc = emitInstruction(AllocaByRef(str))
      val ty = refMem.get(alc) match {
        case Some(s) => s
        case None =>
          refMem += alc -> str
          str
      }
      strTyMem += str -> vexps.map(e => e.ty)
      vexps.indices.foreach(i => {
        val m = emitInstruction(GEP(TRef(ty), alc, ConstantInt(i, i32)))
        if(!vexps(i).ty.isInstanceOf[TStruct]) emitInstruction(Store(vexps(i).ty, vexps(i), m))
      })
      Ref(alc, TRef(ty))

    case KernelF.List(exps @_*) =>
      val vexps = exps.map(compile)
      val ty = TArray(vexps.head.ty, vexps.length)
      val mem = emitInstruction(Alloca(ty))
      vexps.indices.foreach(i => {
        val m = emitInstruction(GEP(ty, mem, ConstantInt(i, i32)))
        emitInstruction(Store(vexps(i).ty, vexps(i), m))
      })
      val res = emitInstruction(Load(ty, mem))
      Ref(res, ty)

    case KernelF.At(col, ind) =>
      col match {
        case vr: KernelF.Var =>
          if(tyMem.contains(vr.x) && tyMem(vr.x).isInstanceOf[TArray]) {
            val vcol = compile(col)
            val res = emitInstruction(ExtractValue(vcol.ty, vcol, compile(ind).asInstanceOf[ConstantInt]))
            Ref(res, tyMem(vr.x).asInstanceOf[TArray].ty)

          } else if(varMem.contains(vr.x)) {
            val t = refMem(varMem(vr.x))
            val idx = compile(ind).asInstanceOf[ConstantInt]
            val ptr = emitInstruction(GEP(TRef(t), varMem(vr.x), idx))
            val res = emitInstruction(Load(strTyMem(t)(ind.i), ptr))
            Ref(res, strTyMem(t)(ind.i))
          } else Void
        case _ =>
          val vcol = compile(col)
          println(tyMem)
          println(varMem)
          vcol.ty match {
            case TRef(n) =>
              if(!n.toString.startsWith("%Struct"))
                throw new IllegalArgumentException(s"At() can only be called on collections. Not $e")
              val idx = compile(ind).asInstanceOf[ConstantInt]
              val ptr = emitInstruction(GEP(vcol.ty, Local(vcol.toString), idx))
              val resTy = strTyMem(Local(n.toString))(ind.i)
              val res = emitInstruction(Load(resTy, ptr))
              Ref(res, resTy)
            case TArray(ty, _) =>
              val res = emitInstruction(ExtractValue(vcol.ty, vcol, compile(ind).asInstanceOf[ConstantInt]))
              Ref(res, ty)
            case e =>
              throw new IllegalArgumentException(s"At() can only be called on collections. Not $e")
          }
      }


    /**  Arithmetic Ops  */
    case KernelF.Add(e1, e2) =>
      val vals = castToFpIf(compile(e1), compile(e2))
      val res = emitInstruction(BinaryOperator(if(vals._1) FAdd else Add, vals._2.ty, vals._2, vals._3))
      Ref(res, vals._2.ty)
    case KernelF.Sub(e1, e2) =>
      val vals = castToFpIf(compile(e1), compile(e2))
      val res = emitInstruction(BinaryOperator(if(vals._1) FSub else Sub, vals._2.ty, vals._2, vals._3))
      Ref(res, vals._2.ty)
    case KernelF.Mul(e1, e2) =>
      val vals = castToFpIf(compile(e1), compile(e2))
      val res = emitInstruction(BinaryOperator(if(vals._1) FMul else Mul, vals._2.ty, vals._2, vals._3))
      Ref(res, vals._2.ty)
    case KernelF.Div(e1, e2) =>
      val vals = castToFpIf(compile(e1), compile(e2))
      val res = emitInstruction(BinaryOperator(if(vals._1) FDiv else Div, vals._2.ty, vals._2, vals._3))
      Ref(res, vals._2.ty)

    /**  Logical Ops  */
    case KernelF.And(e1, e2) =>
      val res = emitInstruction(BinaryOperator(LLVM.And, i32, compile(e1), compile(e2)))
      Ref(res, i32)
    case KernelF.Or(e1, e2) =>
      val res = emitInstruction(BinaryOperator(LLVM.Or, i32, compile(e1), compile(e2)))
      Ref(res, i32)
    case KernelF.Xor(e1, e2) =>
      val res = emitInstruction(BinaryOperator(LLVM.Xor, i32, compile(e1), compile(e2)))
      Ref(res, i32)

    /**  Comparison Ops  */
    case KernelF.Eq(e1, e2) =>
      val vals = castToFpIf(compile(e1), compile(e2))
      val res = emitInstruction(CmpInst(vals._1, if(vals._1) FCMP_OEQ else ICMP_EQ, vals._2.ty, vals._2, vals._3))
      Ref(res, i1)
    case KernelF.NotEq(e1, e2) =>
      val vals = castToFpIf(compile(e1), compile(e2))
      val res = emitInstruction(CmpInst(vals._1, if(vals._1) FCMP_ONE else ICMP_NE, vals._2.ty, vals._2, vals._3))
      Ref(res, i1)
    case KernelF.LT(e1, e2) =>
      val vals = castToFpIf(compile(e1), compile(e2))
      val res = emitInstruction(CmpInst(vals._1, if(vals._1) FCMP_OLT else ICMP_SLT, vals._2.ty, vals._2, vals._3))
      Ref(res, i1)
    case KernelF.LEq(e1, e2) =>
      val vals = castToFpIf(compile(e1), compile(e2))
      val res = emitInstruction(CmpInst(vals._1, if(vals._1) FCMP_OLE else ICMP_SLE, vals._2.ty, vals._2, vals._3))
      Ref(res, i1)
    case KernelF.GT(e1, e2) =>
      compile(KernelF.LEq(e2, e1))
    case KernelF.GEq(e1, e2) =>
      compile(KernelF.LT(e2, e1))

    /**  Control Flow */
    case KernelF.If(cond, thn, els) =>
      val vcond = compile(cond)
      val vthn = compile(thn)
      val vels = compile(els)
      val isTrue = emitInstruction(CmpInst(isFp = false, ICMP_SLT, vcond.ty, ConstantInt(0, vcond.ty), vcond))
      val thnLab = gensym.freshLabel()
      val elsLab = gensym.freshLabel()
      val contLab = gensym.freshLabel()
      endBlock(BrIf(Ref(isTrue, i1), thnLab, elsLab))

      startBlock(thnLab)
      val thnAlt = Alt(vthn, thnLab)
      endBlock(Br(contLab))

      startBlock(elsLab)
      val elsAlt = Alt(vels, elsLab)
      endBlock(Br(contLab))

      startBlock(contLab)
      val res = emitInstruction(Phi(vthn.ty, Seq(thnAlt, elsAlt)))
      Ref(res, vthn.ty)

    case KernelF.Call(name, args @_*) =>
      val vargs = args.map(compile)
      val fname = Global("@" + name)
      val ftype = tyMem.get(fname.toString) match {
        case Some(t) => t
        case None => TVoid
      }
      val res = emitInstruction(Call(FunRef(ftype, fname), vargs))
      Ref(res, ftype)

    /**  Environment  */
    case KernelF.Var(x) =>
      val ty = tyMem.get(x) match {
        case Some(ty) => ty
        case None => TVoid
      }
      ty match {
        case TStruct() =>
          Void
        case _ =>
          varMem.get(x) match {
            case None =>
              Ref(Local("%" + x), ty)
            case Some(mem) =>
              val v = emitInstruction(Load(ty, mem))
              Ref(v, ty)
          }
      }

    case KernelF.Closure(params, body) =>
      Void
  }

  private def castToFpIf(v1: Value, v2: Value): (Boolean, Value, Value) =
    if(v1.ty.isInstanceOf[FloatType] && !v2.ty.isInstanceOf[FloatType]) {
      val res = emitInstruction(CastToFp(v2))
      (true, v1, Ref(res, v1.ty))
    } else if(!v1.ty.isInstanceOf[FloatType] && v2.ty.isInstanceOf[FloatType]) {
      val res = emitInstruction(CastToFp(v1))
      (true, Ref(res, v2.ty), v2)
    } else {
      (v1.ty.isInstanceOf[FloatType], v1, v2)
    }
}
