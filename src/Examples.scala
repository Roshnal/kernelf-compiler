import java.lang.String

import KernelF._
import LLVM.{ConstantInt, Global, TArray, TDouble, TStruct, boolean, i32, i8}

import scala.collection.immutable

object Examples extends App {

  val kfc1 = immutable.Seq(
    Assign("a", Or(BooleanLit(true), BooleanLit(false))),
    Fun(
      "foo",
      immutable.Seq(),
      Mul(NumberLit(3), Var("z"))
    ),
    Assign("b", NumberLit(3.1415)),
    Fun(
      "bar",
      immutable.Seq(("x", TDouble), ("p", TDouble), ("q", i32)),
      Add(
        NumberLit(-15.3),
        Div(Var("x"), Var("p"))
      )
    ),
    If(
      LEq(
        Div(NumberLit(4.254), NumberLit(2)),
        Call("bar", Var("b"), NumberLit(0.1415), NumberLit(-1))
      ),
      Var("a"),
      GT(NumberLit(5), Call("foo", NumberLit(2)))
    )
  )

  val kfc2 = immutable.Seq(
    Fun(
      "foo",
      immutable.Seq(("x", i32), ("y", i32)),
      Mul(Var("y"), Var("x"))
    ),
    Assign("y", NumberLit(2)),
    NumberLit(5159),
    Div(
      Call("foo", Var("y"), Add(NumberLit(10), NumberLit(5))),
      NumberLit(1.146)
    )
  )

  val kfc3 = immutable.Seq(
    Assign("a", NumberLit(5.12)),
    Fun(
      "foo",
      immutable.Seq(("x", TDouble), ("y", TDouble)),
      Add(Var("x"), Var("y"))
    ),
    Mul(NumberLit(2), Call("foo", Var("a"), Var("a")))
  )

  val kfc4 = immutable.Seq(
    Assign("x", If(
      LT(NumberLit(2), NumberLit(5)),
      List(NumberLit(1.5), NumberLit(5.0), NumberLit(6.3)),
      List(NumberLit(8.3), NumberLit(52.1), NumberLit(-19.5))
    )),
    At(Var("x"), IntegerLit(2))
  )

  val kfc5 = immutable.Seq(
    Assign("x", Tuple(RealLit(5.3), IntegerLit(1))),
    //At(Var("x"), Va)
  )

  val res = new CompileKernelF().compileProg(kfc5)
  println(kfc5 + "\n")
  println(res)
}
